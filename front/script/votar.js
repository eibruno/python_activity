const baseUrl = "http://127.0.0.1:8000/";
let voted = 1;
let oldHtml;
$(document).ready(async () => {
    let res;
    await $.ajax({
        type: "POST",
        url: baseUrl + "api/v1/verify/candidates",
        data: "",
        success: (response) => {
            res = response;
        },
        error: (error) => {
            res = error;
        }
    });
    if (res.results != undefined) {
        if (res.results.governor < 2) {
            alert("Temos apenas " + res.results.governor + " governadores cadastrados, favor cadastrar ao menos dois candidatos.");
            window.location.replace("cadastrar.html");
        } else if (res.results.president < 2) {
            alert("Temos apenas " + res.results.president + " presidentes cadastrados, favor cadastrar ao menos dois candidatos.");
            window.location.replace("cadastrar.html");
        }
    }
    oldHtml = $("#hold").html();

    writeType();
});

writeType = () => {
    switch (voted) {
        case 1:
            document.getElementById('tipo').innerHTML = "Presidente";
            break;
        case 2:
            document.getElementById('tipo').innerHTML = "Governador";
            break;
        default:
            break;
    }

}

$(".click").on("click", function (event) {
    if (!isNaN(event.target.value)) {
        if ($("#one").val() == "") {
            $("#one").val(event.target.value);
        } else if ($("#two").val() == "") {
            $("#two").val(event.target.value);
            getInfo(($("#one").val().toString()) + ($("#two").val().toString()), voted);


        } else {
            alert("Favor digitar no máximo dois caracteres, \n pressione CORRIGE para mudar seu voto");
        }
    } else {
        switch (event.target.value) {
            case 'CORRIGE':
                $("#one").val("");
                $("#two").val("");
                $("#name").html("");
                $("#info").html("");
                document.getElementById('picture').src = "https://lh3.googleusercontent.com/-3H3Zmv8LwnU/VHeSXvvTF-I/AAAAAAAABys/7QfuK6hvuqM/w406-h577-no/clarinha-vem.jpg";

                break;
            case 'BRANCO':
                $("#one").val("-");
                $("#two").val("-");
                getInfo(($("#one").val().toString()) + ($("#two").val().toString()), voted);
                break;
            case 'CONFIRMA':

                if (window.confirm("Tem Certeza que deseja votar?")) {
                    confirmVote(`${$("#one").val().toString()}${$("#two").val().toString()}`);
                } else {
                    alert("Abortado");
                }

                break;
            default:
                break;
        }
    }
})

getInfo = async (number) => {
    let res, type;

    switch (voted) {
        case 1:
            type = 'Presidente';
            break;
        case 2:
            type = 'Governador';
            break;
        default:
            break;
    }

    $("#info").html(`
        <p>Nome: <label id="name"></label> </p>
        <p>Partido: <label id="party"></label>  </p>
        <p>Vice: <label id="vice"></p> 
    `)

    await $.ajax({
        type: "POST",
        url: baseUrl + "api/v1/candidate/get",
        data: {
            number,
            type
        },
        success: (response) => {
            res = response;
        },
        error: (error) => {
            res = error;
        }
    });

    if (res.voted !== undefined) {
        document.getElementById('picture').src = res.voted.picture == "-" ? "https://lh3.googleusercontent.com/-3H3Zmv8LwnU/VHeSXvvTF-I/AAAAAAAABys/7QfuK6hvuqM/w406-h577-no/clarinha-vem.jpg" : baseUrl + res.voted.picture;
        document.getElementById('name').innerHTML = res.voted.name;
        document.getElementById('vice').innerHTML = res.voted.vice;
        document.getElementById('party').innerHTML = res.voted.party;
    } else if (res.erro !== undefined) {
        alert(res.erro);
    }
}

confirmVote = async (vote) => {
    $("#hold").html('<div style="font-size: 65px;padding-top: 7rem"><i class="fas fa-spinner fa-spin"></i></div>');
    let res, type;
    console.log('VOTE => ', vote);
    switch (voted) {
        case 1:
            type = 'Presidente';
            break;
        case 2:
            type = 'Governador';
            break;
        default:
            break;
    }
    console.log(vote, type);

    await $.ajax({
        type: "POST",
        url: baseUrl + "api/v1/candidate/vote",
        data: {
            vote,
            type
        },
        success: (response) => {
            res = response;
        },
        error: (error) => {
            res = error;
        }
    });

    if (res.response !== undefined) {

        setTimeout(() => {
            $("#hold").html('<div style="font-size: 24px;padding-top: 7rem">Voto Computado!</div>');
        }, 1300);

        setTimeout(() => {
            $("#hold").html(oldHtml);
            if ((voted + 1) > 2) {
                if (window.confirm("Você já votou para Governador e Presidente, deseja votar novamente?")) {
                    voted = 1;
                    writeType();

                } else {
                    $("#hold").html('<div style="font-size: 24px;padding-top: 7rem">Votação Finalizada! <br /> <input type="button" onclick="getWinner()" value="Ver Resultados!" class="btn btn-success" /> </div>');
                }
            } else {
                voted++;
                writeType();
            }
        }, 3500);


    } else if (res.erro !== undefined) {
        alert(res.erro);
        $("#hold").html(oldHtml);
    }
}

getWinner = async () => {
    let president = Array(),
        governor = Array(),
        res;

    await $.ajax({
        type: "POST",
        url: baseUrl + "api/v1/votes/get/",
        data: '',
        success: (response) => {
            res = response;
        },
        error: (error) => {
            res = error;
        }
    });

    if(res.results !== undefined)
    {
        president = res.results.president;
        governor = res.results.governor;
        totalPresident = res.total.president;
        totalGovernor = res.total.governor;

        president.sort((a,b) => {
            return a.votes > b.votes ? -1 : a.votes < b.votes ? 1 : 0;
        });

        governor.sort((a,b) => {
            return a.votes > b.votes ? -1 : a.votes < b.votes ? 1 : 0;
        });

        let presidentWinner = president.slice(0,1),
            governorWinner = governor.slice(0,1),
            presidentScnd = president.slice(1,2),
            governorScnd = governor.slice(1,2);
        $("#hold").html(`
                        <div style="font-size: 14px;padding-top: 1rem">
                            <br /><br />
                            <b>Presidente Eleito: ${presidentWinner[0].name} (${presidentWinner[0].party}) com ${presidentWinner[0].votes} votos válidos (${((presidentWinner[0].votes/totalPresident)*100).toFixed(2)}%)</b>
                            <br/><br/>Segundo Mais Votado: ${presidentScnd[0].name} (${presidentScnd[0].party}) com ${presidentScnd[0].votes} votos válidos (${((presidentScnd[0].votes/totalPresident)*100).toFixed(2)}%)
                            <br /><br /><br />Total de Votos : ${totalPresident}
                            <br /><br /><hr /><br /><br />

                            <b>Governador Eleito: ${governorWinner[0].name} (${governorWinner[0].party}) com ${governorWinner[0].votes} votos válidos (${((governorWinner[0].votes/totalGovernor)*100).toFixed(2)}%)</b>
                            <br/><br/>Segundo Mais Votado: ${governorScnd[0].name} (${governorScnd[0].party}) com ${governorScnd[0].votes} votos válidos (${((governorScnd[0].votes/totalGovernor)*100).toFixed(2)}%)
                            <br /><br /><br />Total de Votos : ${totalGovernor}
                            <br/> <br /> <input type="button" onclick="tryToClear()" value="Apagar Votos" class="btn btn-success" />
                        </div>
        `);
        
    }

}



tryToClear = async () => {
    if (window.confirm("Tem Certeza que deseja Deletar Todos os Votos?")) {
        let res;
        await $.ajax({
            type: "POST",
            url: baseUrl + "api/v1/votes/delete/",
            cache: false,
            contentType: false,
            processData: false,
            type: 'post',
            success: (response) => {
                res = response;
            },
            error: (error) => {
                res = error;
            }
        });
        if (res !== undefined && res.response !== undefined) {
            alert(res.response);
            window.location.reload();
        }

    } else {
        alert("Abortado.");
    }
}