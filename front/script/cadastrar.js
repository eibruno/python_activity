const baseUrl = "http://127.0.0.1:8000/api/v1/";

tryToSave = async () => {
    let name = $("#name").val(),
        party = $("#party").val(),
        number = $("#number").val(),
        vice = $("#vice").val(),
        picture = document.getElementById('picture'),
        type = $("#type").val();

    if (name === "" || party === "" || vice === "" || number === "" || type === "") { alert("Todos os itens são obrigatórios"); return; }

    if (isNaN(number)) { alert("Numero do candidato deve ser numérico"); return; }

    if (number.length !== 2) { alert("Numero do candidato deve conter 2 digitos"); return; }

    if (window.confirm("Tem certeza que deseja cadastrar este candidato?")) {
        let res;

        if (!picture) { alert("Erro na foto"); return; }
        if (!picture.files) { alert("Erro na foto"); return; }
        if (!picture.files[0]) { alert("Erro na foto"); return; }

        var form_data = new FormData();

        form_data.append('picture', $('#picture').prop('files')[0]);
        form_data.append('name', $("#name").val());
        form_data.append('party', $("#party").val());
        form_data.append('number', $("#number").val());
        form_data.append('vice', $("#vice").val());
        form_data.append('type', $("#type").val());

        await $.ajax({
            type: "POST",
            url: baseUrl + "candidate/new/",
            dataType: 'text',
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'post',
            success: (response) => {
                res = JSON.parse(response);
            },
            error: (error) => {
                res = JSON.parse(error);
            }
        });
        console.log(res);
        if (res.response !== undefined && res.response === "success") {
            alert("Candidato Cadastrado Com Sucesso");
            $("#name").val("");
            $("#party").val("");
            $("#number").val("");
            $("#vice").val("");
            $("#picture").val("");
            $("#type").val("Presidente");
        } else if (res.erro !== undefined) {
            alert(res.erro);
        }

    } else {
        alert("Abortado.");
    }
}

tryToClear = async () => {
    if (window.confirm("Tem Certeza que deseja Deletar Todos os candidatos?")) {
        let res;
        await $.ajax({
            type: "POST",
            url: baseUrl + "candidate/delete/",
            cache: false,
            contentType: false,
            processData: false,
            type: 'post',
            success: (response) => {
                res = response;
            },
            error: (error) => {
                res = JSON.parse(error);
            }
        });
        console.log(res);
        if (res !== undefined && res.response !== undefined) {
            alert(res.response);
            $("#name").val("");
            $("#party").val("");
            $("#number").val("");
            $("#vice").val("");
            $("#picture").val("");
            $("#type").val("Presidente");
        }

    } else {
        alert("Abortado.");
    }
}

goBack = () => {
    window.location.replace("index.html");
}