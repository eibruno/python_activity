const baseUrl = "http://127.0.0.1:8000/api/v1/";

$(document).ready(async () => {
    let res;
    await $.ajax({
        type: "POST",
        url: baseUrl + "verify/",
        data: "",
        success: (response) => {
            res = response;
        },
        error: (error) => {
            res = error;
        }
    });
    if (res.results != undefined && res.results === 0) {
        window.location.replace("cadastrar.html");
    }
});

initVotes = () => {
    window.location.replace("votar.html");
}