	# INSTALE PIP NO TERMINAL
		$ sudo apt-get install python3-pip

	# SIGA AS INSTRUÇÕES ABAIXO PARA INSTALAR O POSTGRES 10.
		# STEP 1: ADICIONE O REPOSITÓRIO DO POSTGRESS
			$ wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
			$ sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt/ bionic-pgdg main" > /etc/apt/sources.list.d/pgdg_bionic.list'
			$ sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt/ xenial-pgdg main" > /etc/apt/sources.list.d/pgdg_xenial.list'

		# STEP2 ATUALIZE A LISTA DE PACOTES E INSTALE O POSTGRESSQL 10
			$ sudo apt update
			$ sudo apt-get install postgresql-10

	# INSTALE PYVENV
		$ sudo apt-get install virtualenv


	# ACESSE POSTGRES COMO SUPER USUÁRIO
		$ sudo su - postgres


	# CRIE UM USUÁRIO NO POSTGRES
		$ createuser --interactive -P
			Enter name of role to add: urna
			Enter password for new role: urna
			Enter it again: urna
			Shall the new role be a superuser? (y/n) n
			Shall the new role be allowed to create databases? (y/n) y
			Shall the new role be allowed to create more new roles? (y/n) n
	
	# CRIE UM BANCO DE DADOS
		$ createdb --owner urna urna

	
	# DIGITE LOGOUT PARA SAIR DO POSTGRES
		$ logout

	# CRIA UM AMBIENTE VIRTUAL
		$ virtualenv backend

	# ATIVE O AMBIENTE VIRTUAL
		$ source backend/bin/activate

	
	# INSTALE DJANGO
		$ python3 -m pip install django

	# INSTALE DJANGO REST
		$ pip3 install djangorestframework
	
	# CASO OCORRA UM ERRO, INSTALE COM O COMANDO PIP
		$ pip install django

	# INSTALE PSYCOPG2
		$ pip3 install psycopg2

	# FAÇA AS MIGRAÇÕES
		$ python3 manage.py makemigrations
		$ python3 manage.py migrate


	# INSTALE CORSHEADERS 
		ACESSE A PASTA urna_backend NO TERMINAL
		EXECUTE O SEGUINTE COMANDO:
		$ sudo pip install django-cors-headers
		
	# INICIE O SERVIDOR NA MESMA PASTA:
		$ python manage.py runserver

	# ABRA O ARQUIVO INDEX.HTML (DENTRO DA FRONT NO DIRETÓRIO RAIZ DO PROJETO) EM UM NAVEGADOR
