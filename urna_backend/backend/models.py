# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from uuid import uuid4

import datetime

class Candidates(models.Model):
    uid = models.UUIDField(primary_key=True, default=uuid4, editable=False)
    name = models.CharField(max_length=200)
    vice_name = models.CharField(max_length=200, null=True)
    number = models.CharField(max_length=2, null=True)
    political_party = models.CharField(max_length=100)
    type_candidate = models.CharField(max_length=100)
    picture = models.CharField(max_length=200)

class Votes(models.Model):
    uid = models.UUIDField(primary_key=True, default=uuid4, editable=False)
    vote = models.CharField(max_length=100, null=True)