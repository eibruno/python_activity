# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.core import exceptions
from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status,  permissions
from . models import Candidates, Votes
from django.db import transaction

class VerifyCandidates(APIView):
    def post(self, request):
        try:
            catchCandidates = Candidates.objects.raw('''SELECT uid, COUNT(*) AS c FROM backend_candidates GROUP BY uid''')
            
            result = 0

            for row in catchCandidates:
                result = row.c
            
            if result == 0:
                candidato = Candidates.objects.create(name='Nulo',political_party='-', vice_name='-', picture='-', number='-',type_candidate='Presidente')
                candidato = Candidates.objects.create(name='Nulo',political_party='-', vice_name='-', picture='-', number='-',type_candidate='Governador')
                candidato = Candidates.objects.create(name='Branco',political_party='-', vice_name='-', picture='-', number='--',type_candidate='Presidente')
                candidato = Candidates.objects.create(name='Branco',political_party='-', vice_name='-', picture='-', number='--',type_candidate='Governador')
            
            catchCandidates = Candidates.objects.raw('''SELECT uid, COUNT(*) AS c FROM backend_candidates WHERE name NOT LIKE 'Nulo' AND name NOT LIKE 'Branco' GROUP BY uid''')
            
            result = 0

            for row in catchCandidates:
                result = row.c
            
            return Response({"results": result})
        except Candidates.DoesNotExist:
            return Response({"error": "not found"})


class VerifyTimesCandidates(APIView):
    def post(self, request):
        try:
            catchCandidates = Candidates.objects.raw('''
                                                        SELECT uid, 
                                                            ( SELECT COUNT(*) AS president FROM backend_candidates WHERE type_candidate='Presidente' AND name NOT LIKE 'Nulo' AND name NOT LIKE 'Branco'),
                                                            ( SELECT COUNT(*) AS governor FROM backend_candidates WHERE type_candidate='Governador' AND name NOT LIKE 'Nulo' AND name NOT LIKE 'Branco')
                                                        FROM backend_candidates 
                                                        GROUP BY uid
                                                     ''')
            
            president = 0
            governor = 0
            for row in catchCandidates:
                president = row.president
                governor = row.governor

            return Response({"results": {"president": president, "governor": governor}})
        except Candidates.DoesNotExist:
            return Response({"error": "not found"})

class RegisterCandidates(APIView):
    def post(self, request):
        name = request.data.get('name')
        party = request.data.get('party')
        number = request.data.get('number')
        picture = request.data.get('picture')
        type_candidate = request.data.get('type')
        vice = request.data.get('vice')

        if name == None or vice == None or party == None or number == None or picture == None or type_candidate == None:
            return Response({"erro": "Você deve enviar alguma informacao"})

        try:
            catchCandidate = Candidates.objects.get(number=number, type_candidate=type_candidate)

            return Response({"erro": "Número de Candidato já Cadastrado no Partido " + party + " como " + type_candidate})

        except Candidates.DoesNotExist:
            candidato = Candidates.objects.create()
            candidato.name = name
            candidato.political_party = party
            candidato.number = number
            candidato.type_candidate = type_candidate

            pathPic = 'media/' + str(candidato.uid) + picture.name.replace('"', '')
            destination = open(pathPic, 'wb+')

            for chunk in picture.chunks():
                destination.write(chunk)
                destination.close()
            candidato.picture = pathPic
            candidato.vice_name = vice
            candidato.save()
            return Response({"response": "success"})

class DeleteCandidates(APIView):
    def post(self,request):
        Candidates.objects.all().delete()
        return Response({"response": "Candidados excluidos com sucesso"})

class DeleteVotes(APIView):
    def post(self,request):
        Votes.objects.all().delete()
        return Response({"response": "Candidados excluidos com sucesso"})


class ConfirmVote(APIView):
    def post(self,request):
        number = request.data.get('vote')
        typed = request.data.get('type')
        if number == None or typed == None:
            return Response({"erro": "Você deve informar o número e o tipo do candidato"})

        try:
            int(number)

            try:
                candidato = Candidates.objects.get(number=number, type_candidate=typed)
                Votes.objects.create(vote=candidato.uid)
                return Response({"response": "success"})
            except Candidates.DoesNotExist:
                candidato = Candidates.objects.get(name='Nulo', type_candidate=typed)
                Votes.objects.create(vote=candidato.uid)
                return Response({"response": "success"})

        except ValueError:
            if(number == "--"):
                candidato = Candidates.objects.get(name='Branco', type_candidate=typed)
                Votes.objects.create(vote=candidato.uid)
                return Response({"response": "success"})
            return Response({"erro": "Número do candidato deve ser Inteiro."})

class GetCandidate(APIView):
    def post(self,request):
        number = request.data.get('number')
        typed = request.data.get('type')

        if number == None or typed == None:
            return Response({"erro": "Você deve informar o número e o tipo do candidato"})

        try:
            int(number)

            try:
                candidato = Candidates.objects.get(number=number, type_candidate=typed)
                return Response({"voted": {"name": candidato.name, "party": candidato.political_party, "vice": candidato.vice_name, "picture": candidato.picture}})
            except Candidates.DoesNotExist:
                return Response({"voted": {"name": "Nulo", "party": "-", "vice": "-", "picture": "-"}})

        except ValueError:
            if(number == "--"):
                return Response({"voted": {"name": "Branco", "party": "-", "vice": "-", "picture": "-"}})
            return Response({"erro": "Número do candidato deve ser Inteiro."})

class GetWinners(APIView):
    def post(self, request):
        presidentList = list()
        governorList = list()
        totalGovernor = 0
        totalPresident = 0
        president = Candidates.objects.raw('''
                                                SELECT
                                                    backend_candidates.uid,
                                                    name,
                                                    political_party,
                                                    number,
                                                    vice_name,
                                                    COUNT(backend_votes.vote) as votes
                                                FROM backend_candidates
                                                    LEFT JOIN backend_votes ON uuid(backend_votes.vote) = backend_candidates.uid
                                                WHERE backend_candidates.type_candidate = 'Presidente'
                                                GROUP BY backend_votes.vote, backend_candidates.name, backend_candidates.political_party, backend_candidates.number,backend_candidates.vice_name, backend_candidates.uid
                                           ''')

        governor = Candidates.objects.raw('''
                                                SELECT
                                                    backend_candidates.uid,
                                                    name,
                                                    political_party,
                                                    number,
                                                    vice_name,
                                                    COUNT(backend_votes.vote) as votes
                                                FROM backend_candidates
                                                    LEFT JOIN backend_votes ON uuid(backend_votes.vote) = backend_candidates.uid
                                                WHERE backend_candidates.type_candidate = 'Governador'
                                                GROUP BY backend_votes.vote, backend_candidates.name, backend_candidates.political_party, backend_candidates.number,backend_candidates.vice_name, backend_candidates.uid
                                           ''')
        for row in president:
            presidentList.append({
                "name": row.name,
                "party": row.political_party,
                "number": row.number,
                "vice": row.vice_name,
                "votes": row.votes,
            })
            totalPresident = totalPresident + row.votes

        for row in governor:
            governorList.append({
                "name": row.name,
                "party": row.political_party,
                "number": row.number,
                "vice": row.vice_name,
                "votes": row.votes,
            })
            totalGovernor = totalGovernor + row.votes
        
        return Response({"results": {"president": presidentList, "governor": governorList}, "total": {"president": totalPresident, "governor": totalGovernor}})
        
