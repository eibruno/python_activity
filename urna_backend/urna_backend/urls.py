"""urna_backend URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from backend import views
from rest_framework.urlpatterns import format_suffix_patterns
from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls import include

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^api/v1/verify/$', views.VerifyCandidates.as_view()),
    url(r'^api/v1/verify/candidates$', views.VerifyTimesCandidates.as_view()),
    url(r'^api/v1/candidate/get$', views.GetCandidate.as_view()),
    url(r'^api/v1/candidate/vote$', views.ConfirmVote.as_view()),
    url(r'^api/v1/candidate/new/$', views.RegisterCandidates.as_view()),
    url(r'^api/v1/candidate/delete/$', views.DeleteCandidates.as_view()),
    url(r'^api/v1/votes/get/$', views.GetWinners.as_view()),
    url(r'^api/v1/votes/delete/$', views.DeleteVotes.as_view()),
]
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)